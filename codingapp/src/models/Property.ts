import { Address } from "./Address";

export interface Property{
    propertyId?: number;
    addressId?: number;
    isNew?: boolean;
    status?: string;
    listPrice?: number;
    yearBuilt?: number;
    monthlyRent?: number;
    grossYield?: number;
    address?: Address;
}