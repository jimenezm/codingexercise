import React from "react";
import { Property } from "../models/Property";

interface Props{
    property: Property,
    onSaveClick: (property: Property) => void
}

export const Row: React.FC<Props> = (props: Props) => {
    
    function getGrossYieldP(value: number){
        let valueP = value * 100;
        return valueP === 0 ? valueP : valueP.toFixed(2);
    }

    function getAddressString(property: Property){
        let address = property.address;
        return address !== undefined ? 
            `${address?.address1}, ${address?.address2 ?? ""}, ${address?.city}, ${address?.state}, ${address?.country}`:
            "";
    }

    return(
        <tr>
            <td>
                <button disabled={Boolean(props.property.propertyId)} type="button" className={`btn ${!Boolean(props.property.propertyId) ? "btn-primary" : "btn-secondary"} btn-sm col`} onClick={(e: any) => props.onSaveClick(props.property)}>Save</button>
            </td>
            <td>{props.property.propertyId}</td>
            <td>{props.property.isNew ? "Yes" : "No"}</td>
            <td>{props.property.status}</td>
            <td>{`$${props.property.listPrice?.toLocaleString('en-us', {minimumFractionDigits: 2}) ?? 0}`}</td>
            <td>{props.property.yearBuilt}</td>
            <td>{`$${props.property.monthlyRent?.toLocaleString('en-us', {minimumFractionDigits: 2}) ?? 0}`}</td>
            <td>{`${getGrossYieldP(props.property.grossYield ?? 0)}%`}</td>
            <td>{getAddressString(props.property)}</td>
        </tr>)
}