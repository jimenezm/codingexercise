import React from "react";
import { Property } from "../models/Property";
import { Row } from "./Row";

interface Props{
    onSaveClick: (property: Property) => void
    properties: Property[];
}

export const Table: React.FC<Props> = (props: Props) => {

    function mapColumns(property: Property){
        return(
            <Row property={property} onSaveClick={props.onSaveClick}/>
        )
    }
    return(
        <div className="table-responsive text-nowrap">
            <table className="table table-dark  table-striped">
                <thead>
                    <tr>
                        <th></th>
                        <th>Id</th>
                        <th>Is New</th>
                        <th>Status</th>
                        <th>List Price</th>
                        <th>Year Built</th>
                        <th>Monthly Rent</th>
                        <th>Gross Yield</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                {props.properties.map(mapColumns)}
                </tbody>
            </table>
        </div>
    );
}