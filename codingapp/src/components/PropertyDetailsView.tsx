import React from "react";
import { Property } from "../models/Property";

interface Props{
    saveProperty: (property: Property) => void;
    onCancel: (value: boolean) => void;
    property: Property | undefined;
    display: boolean
}

export const PropertyDetailsView: React.FC<Props> = (props: Props) =>{

    // this allow me to use states in a stateless component
    const [property, setProperty] = React.useState(props.property);
    
    function validateProperty(){
        // only allows to save a property if all the fields are set
        return Boolean(property?.status && property?.listPrice && property?.monthlyRent && property.yearBuilt
                && (property?.address?.address1 || property?.address?.address2)
                && property?.address?.city && property?.address?.country
                && property?.address?.state && property?.address?.zip);
    }

    function onCancel(){
        setProperty(undefined);
        props.onCancel(false);
    }

    const onSave = async() => {
        await props.saveProperty(property!);
        setProperty(undefined);
    }

    return(
        <div className="container" style={{ display: props.display ? "inline": "none"}}>
            <form className="row g-3">
                <div className="row">
                    <div className="container">
                        <h4>Property Details</h4>
                        <div className="row">
                            <div className="col-3">
                                <label htmlFor="isNewInput" className="form-label">Is New</label>
                                <select className="form-control" id="isNewInput" value={property?.isNew ? "Yes" : "No"} onChange={e => setProperty({...property, isNew: e.target.value === "Yes"})}>
                                    <option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div className="col-3">
                                <label htmlFor="statusInput" className="form-label">Status</label>
                                <input type="text" value={`${property?.status ?? ""}`} className="form-control" id="statusInput" onChange={e => setProperty({...property, status: e.target.value})}/>
                            </div>
                            <div className="col-3">
                                <label htmlFor="yearBuiltInput" className="form-label">Year Built</label>
                                <input type="number" value={`${property?.yearBuilt}`} className="form-control" id="yearBuiltInput" onChange={e => setProperty({...property, yearBuilt: parseInt(e.target.value)})}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-3">
                                <label htmlFor="listPriceInput" className="form-label" placeholder="$">List Price</label>
                                <input type="number" value={`${property?.listPrice}`} className="form-control" id="listPriceInput" onChange={e => setProperty({...property, listPrice: parseFloat(e.target.value)})}/>
                            </div>
                            <div className="col-3">
                                <label htmlFor="monthlyRentInput" className="form-label"  placeholder="$">Monthly Rent</label>
                                <input type="number" value={`${property?.monthlyRent}`} className="form-control" id="monthlyRentInput" onChange={e => setProperty({...property, monthlyRent: parseFloat(e.target.value)})}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <h4>Address</h4>
                        <div className="row">
                            <div className="col-3">
                                <label htmlFor="address1Input" className="form-label">Address 1</label>
                                <input type="text" value={`${property?.address?.address1  ?? ""}`} className="form-control" id="address1Input" onChange={e => setProperty({...property, address:{...property?.address, address1: e.target.value}})}/>
                            </div>
                            <div className="col-3">
                                <label htmlFor="address2Input" className="form-label">Address 2</label>
                                <input type="text" value={`${property?.address?.address2  ?? ""}`} className="form-control" id="address2Input" onChange={e => setProperty({...property, address:{...property?.address, address2: e.target.value}})}/>
                            </div>
                            <div className="col-3">
                                <label htmlFor="cityInput" className="form-label">City</label>
                                <input type="text" value={`${property?.address?.city  ?? ""}`} className="form-control" id="cityInput" onChange={e => setProperty({...property, address:{...property?.address, city: e.target.value}})}/>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-3">
                                <label htmlFor="countryInput" className="form-label">Country</label>
                                <input type="text" value={`${property?.address?.country  ?? ""}`} className="form-control" id="countryInput" onChange={e => setProperty({...property, address:{...property?.address, country: e.target.value}})}/>
                            </div>
                            <div className="col-3">
                                <label htmlFor="stateInput" className="form-label">State</label>
                                <input type="text" value={`${property?.address?.state  ?? ""}`} className="form-control" id="stateInput" onChange={e => setProperty({...property, address:{...property?.address, state: e.target.value}})}/>
                            </div>
                            <div className="col-3">
                                <label htmlFor="zipInput" className="form-label">Zip</label>
                                <input type="text" value={`${property?.address?.zip  ?? ""}`} className="form-control" id="zipInput" onChange={e => setProperty({...property, address:{...property?.address, zip: e.target.value}})}/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-5">
                            <button type="button" className="btn btn-success" disabled={!validateProperty()} onClick={_ => onSave()}>Save</button>
                        </div>
                        <div className="col-5">
                            <button type="button" className="btn btn-danger" onClick={_ => onCancel()}>Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    );
}