import { observable } from "mobx";
import { Property } from "../models/Property";
import PropertiesService from "../services/PropertiesService";

export default class PropertiesViewModel{
    @observable properties: Property[]
    @observable property: Property;
    @observable displayDetails: boolean;
    private propertiesService: PropertiesService;
    

    constructor(){
        this.propertiesService = new PropertiesService();
        this.properties = [];
        this.property = {};
        this.displayDetails = false;

        this.saveProperty = this.saveProperty.bind(this);
    }

    public async loadData(){
        try{
            let data = await this.propertiesService.GetAllProperties();
            this.properties = data;
        }
        catch(error){
            let message = (error && error.message) ? `${error.message}` : "Unknown error";
            alert(message);
        }
    }

    public async saveProperty(property: Property){
        try{
            await this.propertiesService.CreateProperty(property);   
            this.loadData();
        }
        catch(error){
            let message = (error && error.message) ? `${error.message}` : "Unknown error";
            alert(message);
        }
    }

    public onAddPropertyClick(){
        this.property = {
            address: {}
        }
        this.displayDetails = !this.displayDetails;
    }
}