import { observer } from "mobx-react";
import React from "react";
import PropertiesViewModel from "./PropertiesViewModel";
import { PropertyDetailsView } from "./PropertyDetailsView";
import { Table } from "./Table";
interface Props{

}

@observer
export default class PropertiesView extends React.Component<Props>{
    private readonly viewModel = new PropertiesViewModel();

    componentDidMount(){
        // load all the properties after the component was mounted. It is not called in the constructor because 
        // it will called more than once.
        this.viewModel.loadData();
    }

    public render(){
        return(
            <div className="container">
                <button type="button" className="btn btn-primary" onClick={_ => this.viewModel.displayDetails = !this.viewModel.displayDetails}>Add Property</button>
                <Table properties = {this.viewModel.properties} onSaveClick={this.viewModel.saveProperty}/>
                <PropertyDetailsView display={this.viewModel.displayDetails} property={this.viewModel.property} saveProperty={this.viewModel.saveProperty} onCancel={(value: boolean) => this.viewModel.displayDetails = value}/>
            </div>
        )
    }
}