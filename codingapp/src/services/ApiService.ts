export default class ApiService {
    public static Get(url: string): Promise<any>{
        return this.Fetch("GET", url);
    }

    static Post(url: string, data?: any){
        if (data === undefined){
            return this.Fetch("POST", url);
        }
        return this.FetchWithBody("POST", url, data);
    }

    private static async Fetch(httpVerb: string, url: string): Promise<any>{
        let response = await fetch(url, {method: httpVerb});
        return this.ValidateResponse(response);
    }
    
    private static async FetchWithBody(httpVerb: string, url: string, data: any): Promise<any>{
        let body: FormData | string;
        let headers: Record<string, string>;

        body = JSON.stringify(data);
        headers ={
            "Content-Type": "application/json",
        }

        let response = await fetch(url, {
            method: httpVerb,
            headers: headers,
            body: body
        })

        return this.ValidateResponse(response);
    }

    private static async ValidateResponse(response: Response): Promise<any>{
        if (!response.ok){
            let error = await response.text();
            let message = `Error when executing the request. ${response.statusText}. ${response.status}. ${error}`
            return Promise.reject({message: message });
        }

        if (response.status === 200 || response.status === 201){
            return response.json();
        }

        return Promise.resolve();
    }

}