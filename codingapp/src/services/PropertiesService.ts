import { Property } from "../models/Property";
import ApiService from "./ApiService";

export default class PropertiesService{
    private readonly codingServiceUrl: string = "http://localhost:5001";

    private readonly getAllPropertiesUrl: string = `${this.codingServiceUrl}/properties/all`;
    private readonly createPropertyUrl: string = `${this.codingServiceUrl}/properties/create`;
    private readonly updatePropertyUrl: string = `${this.codingServiceUrl}/properties/update/{propertyId}`;
    private readonly loadInitialData: string = `${this.codingServiceUrl}/properties/load`;

    public GetAllProperties(): Promise<Property[]>{
        return ApiService.Get(this.getAllPropertiesUrl);
    }

    public CreateProperty(property: Property): Promise<Property>{
        return ApiService.Post(this.createPropertyUrl, property);
    }

    public LoadInitialData(){
        return ApiService.Post(this.loadInitialData);
    }
}