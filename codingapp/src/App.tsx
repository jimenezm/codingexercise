import React from 'react';
import 'bootstrap/dist/css/bootstrap.css';
import './App.css';
import PropertiesView from './components/PropertiesView';

function App() {
  return (
    <PropertiesView/>
  );
}

export default App;
