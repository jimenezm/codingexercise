﻿namespace Coding.DataServices
{
    using Coding.DataServices.Helpers;
    using Coding.DataServices.Services;
    using Coding.DataServices.Services.Implementation;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class DataServiceRegistrations
    {
        private readonly IServiceCollection serviceCollection;

        public DataServiceRegistrations(IServiceCollection serviceCollection)
        {
            this.serviceCollection = serviceCollection;
        }

        public void Load()
        {
            this.serviceCollection.AddSingleton<IPropertiesDataService, PropertiesDataService>();
            this.serviceCollection.AddSingleton<IPropertiesLoader, PropertiesLoader>();
        }
    }
}
