﻿namespace Coding.DataServices.Mappers
{
    using Coding.DataServices.Models;
    using Coding.DataServices.Models.Response;
    using Coding.DataStore.Models;

    public static class PropertyMapper
    {

        public static PropertyDto ToDto(Property source)
        {
            return new PropertyDto 
            {
                PropertyId = source.PropertyId,
                AddressId = source.AddressId,
                IsNew = source.IsNew,
                Status = source.Status,
                ListPrice = source.ListPrice,
                MonthlyRent = source.MonthlyRent,
                YearBuilt = source.YearBuilt,
                GrossYield = source.ListPrice == 0 ? 0 : (source.MonthlyRent * 12)/source.ListPrice,
                Address = new AddressDto
                {
                    AddressId = source.Address.AddressId,
                    Address1 = source.Address.Address1,
                    Address2 = source.Address.Address2,
                    City = source.Address.City,
                    Country = source.Address.Country,
                    State = source.Address.State,
                    Zip = source.Address.Zip
                }
            };
        }

        public static Property ToModel(PropertyDto source)
        {
            return new Property 
            {
                PropertyId = source.PropertyId,
                AddressId = source.AddressId,
                IsNew = source.IsNew,
                Status = source.Status,
                ListPrice = source.ListPrice,
                MonthlyRent = source.MonthlyRent,
                YearBuilt = source.YearBuilt,
                Address = new Address
                {
                    Address1 = source.Address.Address1,
                    Address2 = source.Address.Address2,
                    City = source.Address.City,
                    Country = source.Address.Country,
                    State = source.Address.State,
                    Zip = source.Address.Zip
                }
            };
        }

        public static PropertyDto FromResponseToDto(PropertyResponse source)
        {
            var listPrice = source.Financial?.ListPrice ?? 0;
            var monthlyRent = source.Financial?.MonthlyRent ?? 0;
            return new PropertyDto
            {
                IsNew = source.IsNew,
                Status = source.Status,
                ListPrice = listPrice,
                MonthlyRent = monthlyRent,
                YearBuilt = source.Physical?.YearBuilt ?? 0,
                GrossYield = listPrice == 0 ? 0 : (monthlyRent * 12) / listPrice,
                Address = GetAddress(source.Address)
            };
        }

        private static AddressDto GetAddress(AddressResponse address)
        {
            if (address == null)
            {
                return null;
            }

            return new AddressDto
            {
                Address1 = address.Address1,
                Address2 = address.Address2,
                City = address.City,
                Country = address.Country,
                State = address.State,
                Zip = address.Zip
            };
        }
    }
}
