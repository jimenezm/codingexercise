﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace Coding.DataServices.Helpers
{
    public static class HttpClientHelper
    {
        public static string ExecuteGetString(string url)
        {
            var client = new HttpClient();
            var result = client.GetStringAsync(new Uri(url)).Result;
            return result;
        }
    }
}
