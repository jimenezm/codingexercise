﻿using Coding.DataServices.Models.Response;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Coding.DataServices.Helpers
{
    public class PropertiesLoader : IPropertiesLoader
    {
        private readonly IConfiguration configuration;

        public PropertiesLoader(IConfiguration configuration)
        {
            /// property file link is in appsettings.json file
            this.configuration = configuration;
        }

        /// <summary>
        /// Reads the file via http. I did it this way to avoid having the file in the source code. 
        /// This method makes use of a HttpHelper that encapsulate a Get method that can be used in other part
        /// of the project if necessary.
        /// </summary>
        public List<PropertyResponse> LoadPropertiesFromFile()
        {
            var url = this.configuration.GetSection("PropertiesFile").Value;
            var stringFile = HttpClientHelper.ExecuteGetString(url);
            var response = JsonConvert.DeserializeObject<FileResponse>(stringFile);
            return response.Properties;
        }
    }
}
