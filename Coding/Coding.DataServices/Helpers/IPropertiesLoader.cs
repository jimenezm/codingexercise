﻿using Coding.DataServices.Models.Response;
using System.Collections.Generic;

namespace Coding.DataServices.Helpers
{
    public interface IPropertiesLoader
    {
        List<PropertyResponse> LoadPropertiesFromFile();
    }
}
