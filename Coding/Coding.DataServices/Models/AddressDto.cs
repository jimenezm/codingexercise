﻿namespace Coding.DataServices.Models
{
    public class AddressDto
    {
		public int AddressId { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }

		public override bool Equals(object obj)
		{
			var source = obj as AddressDto;
			if (source == null)
			{
				return false;
			}

			return this.Equals(source);
		}

		public override int GetHashCode()
		{
			return (this.Address1 ?? string.Empty).GetHashCode() +
				(this.Address2 ?? string.Empty).GetHashCode() +
				(this.City ?? string.Empty).GetHashCode() +
				(this.Country ?? string.Empty).GetHashCode() +
				(this.State ?? string.Empty).GetHashCode() +
				(this.Zip ?? string.Empty).GetHashCode();
		}

		public bool Equals(AddressDto obj)
		{
			return this.Address1 == obj.Address1 &&
				this.Address2 == obj.Address2 &&
				this.City == obj.City &&
				this.Country == obj.Country &&
				this.State == obj.State &&
				this.Zip == obj.Zip;
		}
	}
}
