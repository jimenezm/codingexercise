﻿namespace Coding.DataServices.Models.Response
{
    public class FinancialResponse
    {
        public decimal ListPrice { get; set; }
        public decimal MonthlyRent { get; set; }
    }
}
