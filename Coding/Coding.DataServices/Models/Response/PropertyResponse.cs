﻿namespace Coding.DataServices.Models.Response
{
    public class PropertyResponse
    {
		public bool IsNew { get; set; }
		public string Status { get; set; }
		public PhysicalResponse Physical { get; set; }
		public FinancialResponse Financial { get; set; }
		public AddressResponse Address { get; set; }
	}
}
