﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Coding.DataServices.Models.Response
{
    public class FileResponse
    {
        public List<PropertyResponse> Properties { get; set; }
    }
}
