﻿using System;

namespace Coding.DataServices.Models
{
    public class PropertyDto : IEquatable<PropertyDto>
    {
		public int PropertyId { get; set; }
		public int AddressId { get; set; }
		public bool IsNew { get; set; }
		public string Status { get; set; }
		public decimal ListPrice { get; set; }
		public int YearBuilt { get; set; }
		public decimal MonthlyRent { get; set; }
		public decimal GrossYield { get; set; }

		public AddressDto Address { get; set; }

        public override bool Equals(object obj)
        {
			var source = obj as PropertyDto;
			if (source == null)
            {
				return false;
            }

			return this.Equals(source);
        }

        public override int GetHashCode()
        {
            return this.IsNew.GetHashCode() +
				this.Status.GetHashCode() +
				this.ListPrice.GetHashCode() +
				this.YearBuilt.GetHashCode() +
				this.MonthlyRent.GetHashCode() +
				this.Address.GetHashCode();
		}

        public bool Equals(PropertyDto obj)
        {
			var returned = this.IsNew == obj.IsNew &&
				this.Status == obj.Status &&
				this.ListPrice == obj.ListPrice &&
				this.YearBuilt == obj.YearBuilt &&
				this.MonthlyRent == obj.MonthlyRent &&
				this.Address.Equals(obj.Address);

			return returned;
        }
    }
}
