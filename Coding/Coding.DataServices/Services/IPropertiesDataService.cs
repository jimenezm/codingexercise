﻿using Coding.DataServices.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coding.DataServices.Services
{
    public interface IPropertiesDataService
    {
        IEnumerable<PropertyDto> GetAllProperties();
        PropertyDto AddProperty(PropertyDto property);
    }
}
