﻿namespace Coding.DataServices.Services.Implementation
{
    using Coding.DataServices.Helpers;
    using Coding.DataServices.Mappers;
    using Coding.DataServices.Models;
    using Coding.DataStore.Services;
    using Microsoft.Extensions.Logging;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class PropertiesDataService : IPropertiesDataService
    {
        private readonly IPropertiesModelService propertiesModelService;
        private readonly IPropertiesLoader propertiesLoader;
        private readonly ILogger<PropertiesDataService> logger;

        public PropertiesDataService(IPropertiesModelService propertiesModelService, ILogger<PropertiesDataService> logger, IPropertiesLoader propertiesLoader)
        {
            this.propertiesModelService = propertiesModelService;
            this.propertiesLoader = propertiesLoader;
            this.logger = logger;
        }

        /// <summary>
        ///     Returns the database properties union properties from file that haven't been added yet
        ///     This means that it takes the properties from the file and only returns the ones that are not stored in database.
        /// </summary>
        public IEnumerable<PropertyDto> GetAllProperties()
        {
            var propertiesFromFile = this.propertiesLoader.LoadPropertiesFromFile().Select(PropertyMapper.FromResponseToDto).ToList();
            var properties = this.propertiesModelService.GetProperties().Select(PropertyMapper.ToDto).ToList();
            
            /// This is possible becase PropertyDto override GetHashCode and Equals methods allowing the Contains method to make a correct validation. 
            var missingProperties = propertiesFromFile.Where(x => !properties.Contains(x)).ToList();

            return properties.Union(missingProperties);
        }

        public PropertyDto AddProperty(PropertyDto property)
        {
            try
            {
                var added = this.propertiesModelService.SaveProperty(PropertyMapper.ToModel(property));
                return PropertyMapper.ToDto(added);
            }
            catch(Exception e)
            {
                var message = $"Unable to save Property";
                this.logger.LogError(e, message);
                throw;
            }
        }
    }
}
