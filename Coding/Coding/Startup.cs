﻿namespace Coding
{
    using Coding.DataServices;
    using Coding.DataStore;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    public class Startup
    {
        private const string CorsPolicyName = "AllOriginsAndMethods";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(config => 
            {
                config.EnableEndpointRouting = false;
            }).SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            new DataServiceRegistrations(services).Load();
            new DataStoreRegistrations(services, Configuration).Load();

            // Prevent any Cors error from a browser when calling the service.
            services.AddCors(config =>
            {
                config.AddPolicy(
                    CorsPolicyName,
                    builder => { builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader(); });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.EnvironmentName == "Development")
            {
                app.UseDeveloperExceptionPage();
            }

            app
                .UseCors(CorsPolicyName)
                .UseMvc()
                .UseHttpsRedirection();

        }
    }
}
