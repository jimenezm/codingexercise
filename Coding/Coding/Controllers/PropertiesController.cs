﻿namespace Coding.Controllers
{
    using Coding.DataServices.Models;
    using Coding.DataServices.Services;
    using Coding.DataStore.Models;
    using Microsoft.AspNetCore.Mvc;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;

    [Route("properties")]
    [ApiController]
    public class PropertiesController : ControllerBase
    {
        private readonly IPropertiesDataService propertiesDataService;

        public PropertiesController(IPropertiesDataService propertiesDataService)
        {
            this.propertiesDataService = propertiesDataService;
        }

        
        /// <summary>
        /// Return all the properties from database and json file.
        //  </summary>
        [HttpGet("all")]
        [Produces("application/json")]
        [ProducesResponseType(typeof(List<Property>), (int)HttpStatusCode.OK)]
        public IActionResult GetAllProperties()
        {
            var properties = this.propertiesDataService.GetAllProperties().ToList();
            return this.Ok(properties);
        }

        /// <summary>
        /// Create a new property
        //  </summary>
        [HttpPost("create")]
        [Produces("application/json")]
        public IActionResult CreateProperty([FromBody] PropertyDto property)
        {
            var result = this.propertiesDataService.AddProperty(property);
            return this.Ok(result);
        }
    }
}
