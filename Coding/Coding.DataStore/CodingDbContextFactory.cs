﻿namespace Coding.DataStore
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Factory to create a db context every time is needed. In this way I only have one place in the whole project
    /// where a new db context instance is created. This factory is configured on start up as a singleton.
    /// </summary>
    internal class CodingDbContextFactory
    {
        private readonly DbContextOptions<CodingDbContext> dbContextOptions;

        internal CodingDbContextFactory(DbContextOptions<CodingDbContext> dbContextOptions)
        {
            this.dbContextOptions = dbContextOptions;
        }

        internal CodingDbContext GetDbContext()
        {
            var context = new CodingDbContext(this.dbContextOptions);
            context.Database.SetCommandTimeout(300);
            return context;
        }
    }
}
