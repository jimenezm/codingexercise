﻿namespace Coding.DataStore
{
    using Coding.DataStore.Services;
    using Coding.DataStore.Services.Implementation;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;

    public class DataStoreRegistrations
    {
        private readonly IServiceCollection serviceCollection;
        private readonly IConfiguration configuration;

        public DataStoreRegistrations(IServiceCollection serviceCollection, IConfiguration configuration)
        {
            this.serviceCollection = serviceCollection;
            this.configuration = configuration;
        }

        public void Load()
        {
            this.serviceCollection.AddOptions();

            var codingDbOptions = new DbContextOptionsBuilder<CodingDbContext>()
                .UseSqlServer(this.configuration.GetConnectionString("ConnectionString"))
                .Options;

            var codingDbContextFactory = new CodingDbContextFactory(codingDbOptions);
            this.serviceCollection.AddSingleton(codingDbContextFactory);

            this.serviceCollection.AddSingleton<IPropertiesModelService, PropertiesModelService>();

        }
    }
}
