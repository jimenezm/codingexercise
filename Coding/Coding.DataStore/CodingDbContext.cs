﻿using Coding.DataStore.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Coding.DataStore
{
    internal class CodingDbContext : DbContext
    {
        internal CodingDbContext(DbContextOptions<CodingDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Property> Properties { get; set; } 
        public virtual DbSet<Address> Addresses { get; set; }
    }
}
