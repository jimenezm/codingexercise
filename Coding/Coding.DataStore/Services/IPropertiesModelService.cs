﻿namespace Coding.DataStore.Services
{
    using Coding.DataStore.Models;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public interface IPropertiesModelService
    {
        List<Property> GetProperties();
        Property SaveProperty(Property property);
        void SaveBulkProperties(List<Property> properties);
    }
}
