﻿using Coding.DataStore.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Coding.DataStore.Services.Implementation
{
    /// <summary>
    /// Acts as a repository to manage every operation (get/ create for now) between the data service and the data base
    /// </summary>
    internal class PropertiesModelService : IPropertiesModelService
    {
        private readonly CodingDbContextFactory dbContextFactory;

        public PropertiesModelService(CodingDbContextFactory codingDbContextFactory)
        {
            this.dbContextFactory = codingDbContextFactory;
        }

        public List<Property> GetProperties()
        {
            using (var context = this.dbContextFactory.GetDbContext())
            {
                return context.Properties.Include(x => x.Address).ToList();
            }
        }

        public Property SaveProperty(Property property)
        {
            using(var context = this.dbContextFactory.GetDbContext())
            {
                var address = property.Address;
                context.Addresses.Add(address);
                context.SaveChanges();

                property.AddressId = address.AddressId;
                context.Properties.Add(property);

                context.SaveChanges();

                return property;
            }
        }

        public void SaveBulkProperties(List<Property> properties)
        {
            using (var context = this.dbContextFactory.GetDbContext())
            {
                context.Addresses.AddRange(properties.Select(x => x.Address));
                context.SaveChanges();
                properties.ForEach(x => x.AddressId = x.Address.AddressId);
                context.Properties.AddRange(properties);

                context.SaveChanges();
            }
        }
    }
}
