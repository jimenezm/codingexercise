﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Coding.DataStore.Models
{
    public class Address
    {
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int AddressId { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string Country { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
    }
}
