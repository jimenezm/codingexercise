﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Coding.DataStore.Models
{
	public class Property
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int PropertyId { get; set; }
		public int AddressId { get; set; }
		public bool IsNew { get; set; }
		public string Status { get; set; }
		public decimal ListPrice { get; set; }
		public int YearBuilt { get; set; }
		public decimal MonthlyRent { get; set; }

		[ForeignKey("AddressId")]
		public virtual Address Address { get; set; }
	}
}