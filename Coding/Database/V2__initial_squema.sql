use Coding;

if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'Coding' and TABLE_NAME = 'Addresses')
begin
	create table Addresses 
	(
		AddressId int not null identity primary key,
		Address1 varchar(200),
		Address2 varchar(200),
		City varchar(50),
		Country varchar(50),
		State varchar(20),
		Zip varchar(10)
	);
end;

if not exists (select * from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'Coding' and TABLE_NAME = 'Properties')
begin
	create table Properties
	(
		PropertyId int not null identity primary key,
		AddressId int not null,
		IsNew bit,
		Status varchar(30),
		ListPrice decimal(10,2),
		YearBuilt int,
		MonthlyRent decimal(10,2),

		constraint fk_PropertyAddress foreign key (AddressId)
			references Addresses(AddressId)
	);
end;