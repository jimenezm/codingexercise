**In order to run the projects:**

**Requirements:**
1. .Net Core 3.1 SDK (v3.1.409)
2. .Net 5.0 SDK
3. Node JS
4. Sql Server

**To run:**

**Database set up:**

1. Run the two sql files under Coding/Database in a sql server instance
2. Update the connection string in Coding/Coding/appsetttings.json
3. You will need to update server variable and include, if necessary, user and password details

**Coding:**
1. Go to CodingExercise/Coding/Coding
2. Run "dotnet run" This will start the service on http://localhost:5001/
   
**CodingApp:**
1. Go to CodingExercise/Coding/coddingapp
2. Run npm install
3. After it finishes, run npm start. This will start the app on http:localhost:3000/
